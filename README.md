# Monitoramento de Aplicações com ferramentas **OpenSource**

### **Pré-requisitos:**

- VirtualBox v5.4+
- Vagrant
- Recursos de 3G de memória RAM

### **Conteúdo**

Neste repositório você encontrará o laboratório e a apresentação utilizada na palestra sobre Monitoramento de Aplicações com ferramentas opensource na JoinCommunity 2022 Goiânia-GO.

O arquivo Vagrantfile subirá duas instâncias com para o teste praático de comunicação e monitoramento.

O processo de instalação dos pacotes será todo automatizado através da ferramenta ansible, abaixo seguem os pacotes a serem utilizados:

- apt-transport-https
- ca-certificates
- curl
- gnupg-agent
- software-properties-common
- default-mysql-client
- nfs-common
- snapd
- git
- vim
- docker-ce
- docker-ce-cli
- containerd.io
- docker-compose-plugin
- docker-compose

### Instalação

Para realizar a instalação deste ambiente siga estes passos:

1. Precisamos clonar o repositório para nossa máquina
```
$ git clone https://github.com/4linux/Continuos-Monitoring.git
```
2. Agora acessaremos e vamos inicializar os ambientes:

```
$ cd Continuos-Monitoring
$ vagrant up
```

Aguarde a finalização do processo e após a conclusão poderemos acessar qualquer instância com o comando abaixo:

```
$ vagrant ssh lab1
```
**Ou**
```
$ vagrant ssh lab2
```

### **Ótimos estudos!!**
