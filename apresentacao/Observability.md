# Observability: melhore a análise de suas aplicações!

Saber o comportamento de nossas aplicações tornou-se primordial, mas o comportamento não se resume apenas a saber se a aplicação está funcionando ou se a aplicação está com todas as rotinas em operação.

Ter esse conhecimento dos comportamentos de suas aplicações vão te fornecer informações detalhadas ou suficientes para mostrar análises de fluxo onde uma exceção foi criada, um serviço ou aplicação foi paralizada, e dessa forma você terá a observabilidade. 

## Mas afinal o que é observabilidade?

Na verdade não há um conceito concreto sobre o termo em tecnologia, mas pegando sua origem na matemática, observabilidade segundo Rudolf E. Kálmán: "Dizemos que o sistema é observável, se for possível determinar o estado inicial a partir da observação da saída conhecida, é completa se ocorre em todo intervalo de tempo.", em tecnologia podemos trazer essa teoria dizendo que sabemos como nosso ambiente está analisando seu comportamento enquanto execução e também obtendo informações quando houver exceções.

Monitoramento e testes sempre foram utilizados para lidar com falhas que já são conhecidas e assim conseguir prever o comportamento de serviços e aplicações, porém quando se fala em problemas imprevisíveis esses métodos são pouco eficazes, é neste ponto que entra a observabilidade.

Podemos afirmar que observabilidade é um conceito que define ações em prol da estabilidade de suas aplicações ou serviços, identificando comportamentos anormais e/ou indesejáveis que possam contribuir com informações suficientes para descobrir causas e fluxos que provocaram essas exceções, e quanto mais informações mais observabilidade você terá.

## Mas observabilidade é só conceito?

Para ter uma boa observbilidade são necessários três pilares importantes, ou melhor, são três requisitos que sustentam o conceito, são eles: Logs, Métricas e Tracing.

**Log:** São registros em formato de texto que informam eventos que são produzidos por sistemas ou serviços e indicam acontecimentos em cronologia dos eventos gerados. O armazenamento de Logs são comumente utilizados para centralizar e dar uma visão macro de diversos eventos de diversos sistemas, uma das ferramenta mais utilizadas é a elasticsearch, ela tem como característica o trabalho com dados textuais. A Elasticsearch traz consigo outras ferramentas que compõe a solução para armazenamento e gerência dos logs, como o Kibana, Logstash e o Beats, toda essa solução é conhecida como ELK ou Elastic Stack. Há também outras feramentas que podem ser trabalhadas para a gerência desses logs como Graylog e o Loki.

**Metricas:** São eventos mais relacionados com representações numéricas, como valores quantitativos e armazenados em dados baseados em linha de tempo, exemplificando, são dados como valores de memória, CPU, troughput de rede, números de acessos. No mercado temos algumas soluções como Prometheus, New Relic, DataDog, vale lembrar que temos também o Grafana, mas esse vem para complementar esses softwares fazendo a exibição dinâmica desses valores numéricos em dashboards com gráficos mostrando um visual macro e claro dos números coletados nas métricas.

**Tracing:** Seguindo o propósito de analisar sua aplicação o tracing trabalhará nas minúncias, rastreando todo o fluxo de requisições entre seus serviços e agregando eventos, ou seja, logs das aplicações em um Id corelacionado. Dessa forma, conseguiremos identificar em toda parte do processo o momento em que ocorreu o problema e encontrar a solução de maneira rápida e eficiente. Um das ferramentas mais conhecidas no mercado é o Jeager e o Dynatrace, e mais uma vez o Grafana está no processo consolidando e integrando os dados para uma visualização melhor das informações do tracing.

## Ferramentas para observabilidade

Seguindo então nosso conceito, para garantirmos o cumprimento dos pilares da observabilidade são necessários ferramentas que nos ajudam na identificação holística da saúde de nossos ambientes, no mercado possuímos uma diversidade grande de soluções que oferece de diferentes modos o que buscamos.

Um conjunto de soluções confiável poderá trazer auxílio e facilidade em suas análises, então a escolha das ferramentas devem seguir os requisitos de cada ambiente e de cada particularidade que você necessita de observar, mas podemos garantir o mínimo de observabilidade tendo pelo menos uma ferramenta para cada pilar, abaixo seguem algumas mais utilizadas:

**Elastic Stack ou ELK Stack**: Composto de quatro projetos opensource, Elasticsearch, Logstash, Beats e Kibana. O Elasticsearch é um mecanismo de busca e análise. O Logstash é um pipeline de processamento de dados do lado do servidor que faz a ingestão de dados a partir de inúmeras fontes simultaneamente, transforma-os e envia-os para um centralizador como o Elasticsearch. O Kibana permite que os usuários visualizem dados com diagramas e gráficos no Elasticsearch. Os Beats são excelentes para reunir dados. Eles ficam nos seus servidores, com os seus containers, ou são implantados como funções — e, então, centralizam os dados no Elasticsearch. Essa composição é ótima para gerenciamento dos seus logs, além de centralizar e dar uma visão holística do seu ambiente. Para mais informações consulte https://www.elastic.co/pt/elastic-stack/

**Prometheus**: Prometheus, um projeto Cloud Native Computing Foundation, é um sistema de monitoramento de sistemas e serviços. Ele coleta métricas de destinos configurados em determinados intervalos, avalia expressões de regras, exibe os resultados e pode acionar alertas quando condições especificadas são observadas. O monitoramento da saúde do seu ambiente é importantíssimo, e o prometheus consegue te entregar através das coletas de métricas informações de suas aplicações, containers, microserviços, clusters e recursos computacionais, através de seus exporters ele consegue obter métricas de uma infinidade de aplicações e suas diversas linguagens de programações, algumas já possuem nativamente o monitoramento disposto para o prometheus. Para mais informações consulte https://github.com/prometheus/prometheus

**Elastic APM**: Application Performance Monitoring ou monitoramento de performace de aplicações, o Application Performance Monitoring resolve o abismo entre as métricas e os registros. Os registros e métricas têm mais a ver com infraestrutura e componentes, enquanto o APM se concentra nos aplicativos e permite que profissionais de TI e desenvolvedores monitorem a camada da aplicação do stack, incluindo a experiência do usuário final. Informações sensíveis como tempo gasto pela sua aplicação para exibir determinada funcionalidade, ver como serviços interagem entre si, agir preventivamente em gargalos e erros de performance antes que sua aplicação pare de funcionar. Para mais informações consulte https://www.elastic.co/pt/blog/monitoring-applications-with-elasticsearch-and-elastic-apm

**Grafana:** O Grafana permite consultar, visualizar, alertar e entender suas métricas, não importa onde elas estejam armazenadas. A forma como os dados são coletados são puro texto e o grafana vem para tornar a exibição desses dados mais clara, exibindo dashboards com gráficos e indicativos de visuais, ele também oferece a funcionalidade de alertas analisando as métricas e disparando notificações em uma variedade incrível de servicços de mensageria. Para mais informações consulte https://github.com/grafana/grafana

## Por fim...

Seguindo esses pilares teremos então uma ótima base para observabilidade, lembrando que é conceito, portanto sua utilização demanda das necessidades de cada requisito, cada aplicação e cada ambiente. 
O entendimento das ferramentas e seus processos poderá trazer um melhor aproveitamento, nós da 4Linux promovemos diversas formações que explora a fundo todas essas ferramentas, assim você levará ao máximo a utilização e performará ainda mais seu ambiente criando processos e implantando a observabilidade em seu ambiente e a cultura em sua empresa:

https://4linux.com.br/cursos/treinamento/especialista-elastic-stack-elasticsearch-logstash-beats-e-kibana/

https://4linux.com.br/cursos/treinamento/praticas-de-continuous-monitoring-para-uma-infraestrutura-agil/






